#include <iostream>
#include <fstream>
#include <iomanip>
#include <string>
#include <cmath>
#include <algorithm>
#include <opencv2/opencv.hpp>

using namespace std;
using namespace cv;

int main() {
    string path_in;
    cin >> path_in;

    string path_out;
    cin >> path_out;

    // Read the input image
    Mat input_image = imread(path_in, IMREAD_GRAYSCALE);
    if (input_image.empty()) {
        cerr << "Error: Could not open or find the image: " << path_in << endl;
        return -1;
    }

    // Apply the Canny edge detector
    Mat output_image;
    double low_threshold = 50;
    double high_threshold = 150;
    int kernel_size = 3;

    Canny(input_image, output_image, low_threshold, high_threshold, kernel_size);

    // Save the output image
    imwrite(path_out, output_image);

    return 0;
}

/*This code uses the OpenCV library to perform the Canny edge detection. To compile and run the code, you need to install OpenCV and link the appropriate libraries. Here's a sample command for compilation with g++:

g++ canny_edge_detector.cpp -o canny_edge_detector `pkg-config --cflags --libs opencv`

To use the program, provide the input and output file paths:

./canny_edge_detector input_image_path output_image_path

For a CUDA implementation of the Canny edge detector, you can refer to NVIDIA's NPP library, which includes an optimized Canny edge detection function. The documentation can be found here: NVIDIA Performance Primitives (NPP) Library*/
