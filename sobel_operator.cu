#include <iostream>
#include <fstream>
#include <iomanip>
#include <string>
#include <cmath>
#include <algorithm>

#define uint unsigned int

#define DEBUG true
#define DEBUG_MATRIX false
#define DEBUG_TIMER true

using namespace std;

void print(int *matrix, int w, int h) {
  for (int i = 0; i < h; i++) {
    for (int j = 0; j < w; j++) {
      int c = matrix[i * w + j];
      cout << setw(4) << (int)(c & 0xff) << ",";
      cout << setw(4) << (int)((c >> 8) & 0xff) << ",";
      cout << setw(4) << (int)((c >> 16) & 0xff) << ",";
      cout << setw(4) << (int)((c >> 24) & 0xff) << "\t";
    }
    cout << endl;
  }
}

__device__ int get_element(int *input, int w, int h, int i, int j) {
  i = max(0, min(i, h - 1));
  j = max(0, min(j, w - 1));
  return input[i * w + j];
}

__device__ int pack(int4 rgba) {
  return (rgba.w << 24) | (rgba.z << 16) | (rgba.y << 8) | rgba.x;
}

__device__ int4 unpack(int c) {
  int4 rgba;
  rgba.x = c & 0xff;
  rgba.y = (c >> 8) & 0xff;
  rgba.z = (c >> 16) & 0xff;
  rgba.w = (c >> 24) & 0xff;
  return rgba;
}

__device__ double intensity(int4 rgba) {
  return 0.299 * rgba.x + 0.587 * rgba.y + 0.114 * rgba.z;
}

__global__ void sobel_operator_kernel(int *input, int *output, int w, int h) {
  int idx = blockIdx.x * blockDim.x + threadIdx.x;
  int idy = blockIdx.y * blockDim.y + threadIdx.y;
  int offsetx = gridDim.x * blockDim.x;
  int offsety = gridDim.y * blockDim.y;

  int Gx[3][3] = {{-1, 0, 1},
                  {-2, 0, 2},
                  {-1, 0, 1}};

  int Gy[3][3] = {{-1, -2, -1},
                  {0,  0,  0},
                  {1,  2,  1}};

  for (int i = idx; i < h; i += offsetx) {
    for (int j = idy; j < w; j += offsety) {
      double gx = 0;
      double gy = 0;

      for (int x = -1; x <= 1; x++) {
        for (int y = -1; y <= 1; y++) {
          int4 rgba = unpack(get_element(input, w, h, i + x, j + y));
          double I = intensity(rgba);
          gx += Gx[x + 1][y + 1] * I;
          gy += Gy[x + 1][y + 1] * I;
        }
      }

      int result = min(255, (int)sqrt(gx * gx + gy * gy));
    // writing intensity to RGB
    output[i * w + j] = pack(make_int4(result, result, result, 0));
    }
  }
}

void sobel_operator_gpu(int *input, int *output, int w, int h) {
dim3 BLOCKS(16, 16);
dim3 THREADS(32, 32);

int *dev_input;
int *dev_output;
cudaMalloc(&dev_input, sizeof(int) * w * h);
cudaMalloc(&dev_output, sizeof(int) * w * h);
cudaMemcpy(dev_input, input, sizeof(int) * w * h, cudaMemcpyHostToDevice);
cudaMemcpy(dev_output, output, sizeof(int) * w * h, cudaMemcpyHostToDevice);

cudaEvent_t start, stop;

if (DEBUG && DEBUG_TIMER) {
cudaEventCreate(&start);
cudaEventCreate(&stop);
cudaEventRecord(start, 0);
}

sobel_operator_kernel<<<BLOCKS, THREADS>>>(dev_input, dev_output, w, h);

if (DEBUG && DEBUG_TIMER) {
cudaGetLastError();
cudaEventRecord(stop, 0);
cudaEventSynchronize(stop);
float t;
cudaEventElapsedTime(&t, start, stop);
cout << t << endl;
cudaEventDestroy(start);
cudaEventDestroy(stop);
}

cudaMemcpy(output, dev_output, sizeof(int) * w * h, cudaMemcpyDeviceToHost);
cudaFree(dev_input);
cudaFree(dev_output);
}

int main() {
string path_in;
cin >> path_in;

string path_out;
cin >> path_out;

int w, h;
ifstream input_stream;
input_stream.open(path_in.c_str(), ios::binary);

// read the width and height of the image from the input file
input_stream.read((char *)&w, sizeof(int));
input_stream.read((char *)&h, sizeof(int));

// Input and output images
int *input = new int[w * h];
int *output = new int[w * h];

input_stream.read((char *)input, sizeof(int) * w * h);
input_stream.close();

if (DEBUG && DEBUG_MATRIX) {
cout << "Before:" << endl;
print(input, w, h);
cout << endl;
}

sobel_operator_gpu(input, output, w, h);

if (DEBUG && DEBUG_MATRIX) {
cout << "After:" << endl;
print(output, w, h);
cout << endl;
}

// writing to a file
ofstream output_stream;
output_stream.open(path_out.c_str(), ios::binary);
output_stream.write((char *)&w, sizeof(int));
output_stream.write((char *)&h, sizeof(int));
output_stream.write((char *)output, sizeof(int) * w * h);
output_stream.close();

delete[] input;
delete[] output;

return 0;
}

